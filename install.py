#! /usr/bin/python3
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Installing symspam
#
# Usage: PYTHONPATH="lib" ./install.py 
#

import subprocess
import os

VERSION = '1.1.x'

# 1.1.0 => 1.1
vermaj   = '.'.join(VERSION.split('.')[0:2])
PREFIX   = '/usr/local/symspam/'
PREFIXPY = PREFIX + vermaj + '/'
BINPY    = PREFIXPY + 'bin/'
LIBPY    = PREFIXPY + 'lib/'

PREFIXSH = '/usr/local/'
BINSH    = PREFIXSH + 'bin/'

BINFILESPY  = ['symspam_wl.py', 'symspam_err.py', 'symspam_ban.py' ]
LIBFILESPY  = ['erreur.py','symspamdate.py','symspamql.py',
              'symspamtables.py','banlist.py','banmgr.py','errstats.py']
BINFILESBASH= ['symspam_wl', 'symspam_err', 'symspam_ban' ]

# Create the directories
for d in [PREFIX,PREFIXSH, PREFIXPY, BINSH, BINPY, LIBPY ]:
	try:
		os.mkdir(d)
	except FileExistsError as e:
		pass

# Create bash wrappers
for f in BINFILESBASH:
	fn = BINSH + f
	fpy= BINPY + f + '.py'
	with open(fn, 'w') as f:
		f.write("#! /bin/bash\n")
		f.write("PYTHONPATH=" + LIBPY + " python3 " + fpy + " $*\n")

	os.chmod(fn,0o500)

# Install py bin files
print ("Copie dans " + BINPY)
src = ' '.join([ 'bin/' + f for f in BINFILESPY ])
cmd = 'install -o root -m 400 -t ' + BINPY + ' '+ src
subprocess.run(cmd, shell=True)

# Install py lib files
print ("Copie dans " + LIBPY)
src = ' '.join([ 'lib/' + f for f in LIBFILESPY ])
cmd = 'install -o root -m 400 -t ' + LIBPY + ' ' + src
subprocess.run(cmd, shell=True)

print ("That'all Folks !")
