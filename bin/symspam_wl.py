#! /usr/bin/python
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

from symspamql import *
from symspamdate import Date
from symspamtables import SymSpamTables,VERSION
import re

import argparse

def read_mailinfo(fn,date):
	'''Read the mail system log file, return a dict: key = mail address, val = number of mails corrrectly sent'''

	if fn == None:
		raise Exception("Donnez un nom de fichier !")
		
	date_str = date.getFormatted('%b %d').replace(' 0','  ')	#Jan 01 => Jan  1
	addresses = {}
	pattern = '^'+date_str+'.+to=<(.+)>,.+ status=sent '
	regex = re.compile(pattern)
	line_nb = 0;
	with open(fn,'r') as f:
		while True:
			line_nb += 1
			# Detecting errors in the mail log file
			l = ""
			try:
				l = f.readline()
			except Exception as e:
				print ("ATTENTION - Erreur de lecture rencontrée fichier = {0} - ligne = {1} - ligne ignorée".format(fn,line_nb))
				print (e)
				print ("======")
				continue
				
			if l == '':
				break
			m = regex.match(l)
			if m != None:
				addr = m.group(1)
				if addr in addresses:
					addresses[addr] += 1
				else:
					addresses[addr] = 1
	return addresses
	
def store_in_db(symsp,date,wl):
	'''Store the whitelist to the symspam database and return the number of lines inserted'''

	cpt = 0
	date_iso = date.getIsoformat()
	for a in wl:
		try:
			symsp.request("INSERT INTO `whitelist` (date,counter,target_email) VALUES (%s,%s,%s)",(date_iso,wl[a],a))
			cpt += 1
		except mysql.connector.errors.IntegrityError:
			pass

		if cpt % 100 == 0:
			symsp.commit()

	symsp.commit()	
	return cpt

def del_from_db(symsp,date):
	'''Remove from the table the rows whose date is older than date'''
	
	symsp.request("DELETE FROM `whitelist` WHERE date<%s",(date.getIsoformat(),))
	symsp.commit()
	
def only_subscribed(sympa,wl):
	'''Remove from wl the addresses which are not in the sympa subscriber list'''

	tab = [t[0] for t in sympa.request("SELECT user_subscriber FROM subscriber_table WHERE robot_subscriber != ''")]
	wo = {}
	for a in wl:
		if a in tab:
			wo[a] = wl[a]
	return wo
	
def params():
	'''Read the cli parameters'''

	parser = argparse.ArgumentParser(description="symspam_wl version "+ VERSION + " - Build the whitelist, ie the addresses to whom we could send a mail today")
	parser.add_argument('--version', action='version', version='%(prog)s '+ VERSION)
	parser.add_argument('--file  ', action="store", type=str,default=None,dest="file",help="The name of the mail.info file")
	parser.add_argument('--date  ',    action="store", type=str,default=None,dest="date",help="Create the whitelist for DATE (2020-03-15, def=today)")
	parser.add_argument('--delete',  action="store", type=int,default=None,dest="delete",help="Delete addresses older than DELETE days before date")

	options = parser.parse_args()
	return options

# main program
options = params()
mailinfo= options.file
date_opt= options.date
delete  = options.delete

if date_opt == None:
	date = Date()
else:
	date = Date(date_opt)

_symsp = Mysql("SYMSPAM")
_sympa = Mysql("SYMPARO")

st     = SymSpamTables(_symsp)

if delete != None and delete > 0:
	date_lim = date.getOtherDay(-delete)
	del_from_db(_symsp,date_lim)

if mailinfo != None:	
	wl    = read_mailinfo(mailinfo,date)
	wl    = only_subscribed(_sympa,wl)
	store_in_db(_symsp,date,wl)
