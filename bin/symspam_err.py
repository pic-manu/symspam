#! /usr/bin/python
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

from symspamconf import getConf
from symspamql import *
from symspamdate import Date
from banlist import BanList,DAY
from symspamtables import SymSpamTables,VERSION
from errstats import ErrStats
from erreur import PicErreur

import argparse

def params():
	'''Read the cli parameters'''

	parser = argparse.ArgumentParser(description="symspam_err version "+ VERSION + " - Counts and stores the errors")
	parser.add_argument('--version', action='version', version='%(prog)s '+ VERSION)
	parser.add_argument('--date', action="store", type=str,default=None,dest="date",help="Compute the errstats at this date - format is 2020-03-15, default is today")
	
	options = parser.parse_args()
	return options
	

# main program
options = params()
date    = Date(options.date)

_sympa = Mysql("SYMPARO")
_symsp = Mysql("SYMSPAM")
st     = SymSpamTables(_symsp)

es     = ErrStats(_sympa,_symsp)
es.addErrStats(date)
