#! /usr/bin/python
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

from symspamql import Mysql
from symspamdate import Date
from banmgr import BanMgr
from symspamtables import SymSpamTables,VERSION
from errstats import ErrStats
from erreur import PicErreur

import argparse

def params():
	'''Read the cli parameters'''

	parser = argparse.ArgumentParser(description="symspam_ban version "+ VERSION + " - Bans and unbans addresses")
	parser.add_argument('--version', action='version', version='%(prog)s '+ VERSION)
	parser.add_argument('--period', action="store", type=str,default=None,dest="period",help="The period in days. 1 for daily, 7 for weekly etc.")
	
	options = parser.parse_args()
	return options
	

# main program
options = params()
period  = int(options.period)

_sympa = Mysql("SYMPARW")
_symsp = Mysql("SYMSPAM")

st     = SymSpamTables(_symsp)

today  = Date()
begin  = today.getOtherDay(-period)
un     = 0

bm = BanMgr(_sympa,_symsp, begin, today)
un += bm.unbanSuccess()
un += bm.synchro()
(temp,perm) = bm.banErrors()

# Close and open sympa, readonly
_sympa = None
_sympa = Mysql("SYMPARO")

# Add stats
es = ErrStats(_sympa,_symsp)
es.addBanStats(temp+perm, un, today)

