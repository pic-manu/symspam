# symspam

Si en tant que plateforme sympa vous voulez que votre "e-reputation" ne soit pas trop dégradée, vous devez nettoyer vos listes sympa. Ce programme est censé vous aider à faire cette opération: il repère les adresses en erreur dans une installation sympa (tous robots virtuels confondus) et "bannit" sur des périodes de temps plus ou moins longues les adresses en erreur.

"bannir" signifie suspendre l'abonnement (le courrier n'est plus envoyé), l'utilisateur ou le propriétaire pourra aisément remettre l'adresse en service après résolution du problème.

*Il faut être root pour installer ou utiliser ce programme*

Pour installer:

```
python3 install.py
```


Pour utiliser : Mettre les lignes suivantes dans le crontab de root (à adapter évidemment)

```
# symspam - la whitelist = Les succes du jour !
55 23 * * * /usr/local/bin/symspam_wl --file /var/log/mail.info --delete 60

# symspam - les stats = Compter les echecs du jour
57 23 * * * /usr/local/bin/symspam_err

# symspam - les adresses suspendues = Une fois par semaine
05 00 * * 6 /usr/local/bin/symspam_ban --period 7
```

