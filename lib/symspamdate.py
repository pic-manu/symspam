#! /usr/bin/python3
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import datetime
import re

from erreur import PicErreur


class Date(object):
	__date = None
	
	def __init__(self,date=None):
		if date == None:
			d           = datetime.datetime.today()
			t           = datetime.time(0,0,0)
			self.__date = datetime.datetime.combine(d,t)
			
		elif isinstance(date,datetime.datetime):
			t           = datetime.time(0,0,0)
			self.__date = datetime.datetime.combine(date,t)
		
		elif isinstance(date,datetime.date):
			self.__date = datetime.datetime(date.year,date.month,date.day)
			
		elif isinstance(date,int):
			'''It is a timestamp: set the datetime to same date, 00:00:00'''
			d           = datetime.date.fromtimestamp(date)
			t           = datetime.time(0,0,0)
			self.__date = datetime.datetime.combine(d,t)

		elif isinstance(date,str):
			'''it is a date, should be iso formatted: YYYY-MM-DD'''

			d = re.match('(\d\d\d\d)-(\d?\d)-(\d?\d)',date) 
			if d != None:
				try:
					self.__date = datetime.datetime(int(d.group(1)),int(d.group(2)),int(d.group(3)))
				except ValueError as e:
					raise PicErreur("ERREUR - " + date + " n'est pas au format YYYY-MM-DD") 
			else:
				raise PicErreur("ERREUR - " + date + " n'est pas au format YYYY-MM-DD") 
		
		else:
			raise PicErreur("ERREUR - " + str(date) + " n'est pas une date")
			
	def getIsoformat(self):
		return self.__date.isoformat().partition('T')[0]
		
	def getTimestamp(self):
		return int(self.__date.timestamp())
		
	def getFormatted(self, fmt):
		return self.__date.strftime(fmt)
		
	def getOtherDay(self,n):
		'''Return a Date object, n days in the future or in the past'''
		delta = datetime.timedelta(days=n)
		d     = self.__date + delta
		return Date(d)

	def getDate(self):
		return self.__date
