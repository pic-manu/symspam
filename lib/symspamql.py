#! /usr/bin/python3
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Usage = symerrstat.py --from=yyyymmdd --to=yyyymmdd
#

from symspamconf import getConf
import mysql.connector
import os
from erreur import PicErreur


KNOWN_PREFIXES = [ 'SYMPARW', 'SYMPARO', 'SYMSPAM' ]

# The class Mysql encapsulates one of the three connections to the databases: sympa readonly or sympa read-write, or symspam 
# It has the "knowledge" of the connection type (see isSympa() and isReadOnly()
class Mysql(object):
	
	__db     = None
	__cursor = None
	
	def __init__(self,prefix):
		'''Open the connection to the db, using parameters in sympsam.conf
		   The prefix is used to compute the parameter names'''
		   
		if not prefix in KNOWN_PREFIXES:
			raise PicErreur("ERREUR INTERNE - Le prefixe " + prefix + " est inconnu")
			
		self.__db = mysql.connector.connect(
			host=getConf(prefix + "_DBHOST"),
			user=getConf(prefix + "_DBUSER"),
			passwd=getConf(prefix + "_DBPASS"),
			database=getConf(prefix + "_DB")
		);
		self.__cursor = self.__db.cursor()

		if prefix == 'SYMPARW':
			self.__ro    = False
			self.__sympa = True

		if prefix == 'SYMPARO':
			self.__ro    = True
			self.__sympa = True

		if prefix == 'SYMSPAM':
			self.__ro    = False
			self.__sympa = False
		
	def __del__(self):
		if self.__db != None:
			self.__db.close()

	def isReadonly(self):
		return self.__ro
		
	def isSympa(self):
		return self.__sympa
		 
	def request(self,request,prms=None):
		if prms == None:
			self.__cursor.execute(request)
		else:
			self.__cursor.execute(request,prms)
			
		if (self.__cursor.with_rows):
			result = self.__cursor.fetchall()
		else:
			result = None
			
		if os.getenv('SYMSPAM_DEBUG')=="1":
			print ("REQUEST = " + request.replace('\t',''))
			if prms != None:
				print ("PRMS    = " + str(prms))
			print ("RESULT  = " + str(result))
		return result

	def commit(self):
		self.__db.commit()
