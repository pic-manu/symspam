#! /usr/bin/python3
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

from symspamql import Mysql
from symspamdate import Date
from banlist import BanList
from erreur import PicErreur

PERMANENT_ERRORS = { '5.1.1','5.1.2','5.1.3' }
TEMPORARY_ERRORS = { '4.2.2','5.2.2','4.4.1' }

#
# BanMgr= This class reads the sympa logs and manages the bans (unban some and ban others)
#
class BanMgr(object):
	
	__bl           = None
	__symsp        = None
	__sympa        = None

	__start_ts     = None
	__end_ts       = None

	__white_list   = None
	__error_tuples = None
	
	def __init__(self,sympa,symspam,start_date, end_date):
		
		self.__symsp = symspam
		self.__sympa = sympa
		self.__bl    = BanList(sympa,symspam)
		
		self.__start_ts = start_date.getTimestamp()
		self.__end_ts   = end_date.getTimestamp()
		self.__start_iso= start_date.getIsoformat()
		self.__end_iso  = end_date.getIsoformat()
		today_ts        = Date().getTimestamp()
		
		if self.__start_ts >= self.__end_ts:
			raise PicErreur ("ERREUR avec start_date (" + start_date + ") et end_date (" + end_date + ")")
		
		if self.__start_ts >= today_ts or self.__end_ts > today_ts:
			raise PicErreur ("ERREUR avec start_date (" + start_date.getIsoformat() + ") ou end_date (" + end_date.getIsoformat() + ")")
			
		self.__buildWhiteList()
		self.__filterErrors()

	def synchro(self):
		return self.__bl.synchro()
		
	def __buildWhiteList(self):
		'''Read the table white_list, built by symspam_wl.py, and build the set self.__white_list'''
		
		#request = "SELECT `target_email_logs` FROM `logs_table` WHERE `status_logs`='success' AND `date_logs` >= %s AND `date_logs` <  %s "
		request  = "SELECT `target_email`FROM `whitelist`WHERE `date` >= %s AND `date` <  %s "
		s_tuples= self.__symsp.request(request,(self.__start_iso,self.__end_iso))
		s_addr  = [ s[0] for s in s_tuples ] 
		self.__white_list = set(s_addr)
	
	def __filterErrors(self):
		'''Read the records marked error and bounced
		   If the address is in __white_list, ignore it
		   Build and return a hash formatted as follows:
		   permanent|temporary|ignored -> my_robot -> my_list -> my_email -> error1 -> count
		                                                                     error2 -> count    
		   '''
		
		if self.__white_list == None:
			raise PicErreur("ERREUR - __successful_addr n'est PAS initialisé")
		
		request = "SELECT `date_logs`,`robot_logs`,`list_logs`,`target_email_logs`,`error_type_logs` FROM `logs_table` WHERE `date_logs` >= %s AND `date_logs` <  %s AND `daemon_logs` = 'bounced'" 
		
		fltrd_tmp = self.__sympa.request(request,(self.__start_ts,self.__end_ts))
		fltrd = {}
		for t in fltrd_tmp:
			if t[3] not in self.__white_list:
				date  = t[0]
				robot = t[1]
				liste = t[2]
				email = t[3]
				error = t[4]
				
				type_err = 'ignored'
				if error in PERMANENT_ERRORS:
					type_err = 'permanent'
				if error in TEMPORARY_ERRORS:
					type_err = 'temporary' 

				if fltrd.get(type_err) == None:
					fltrd[type_err] = {}
				
				t_fltrd = fltrd[type_err]

				if t_fltrd.get(robot) == None:
					t_fltrd[robot] = {}
				
				r_fltrd = t_fltrd[robot]
				
				if r_fltrd.get(liste) == None:
					r_fltrd[liste] = {}
					
				l_fltrd = r_fltrd[liste]
				
				if l_fltrd.get(email) == None:
					l_fltrd[email] = {}
				
				m_fltrd = l_fltrd[email]
				
				if m_fltrd.get(error) == None:
					m_fltrd[error] = 0

				m_fltrd[error] += 1
		
		self.__error_tuples = fltrd
		return fltrd

	def unbanSuccess(self):
		'''Unban the previously banned tuples whose address is in the white list and return the number of unbanned tuples 
		   NOTE - We delete the lines from the symspan table, without calling unbanTerm. '''

		banned_addr   = self.__symsp.request("SELECT DISTINCT target_email FROM `ban`")
		if len(banned_addr) > 0:
			banned_addr_s = set([b[0] for b in banned_addr])
			to_del_s      = self.__white_list.intersection(banned_addr_s)
			if len(to_del_s) > 0:
				to_del        = "(" + ",".join(["'"+s+"'" for s in to_del_s]) + ")"
				#request       = "DELETE FROM ban WHERE target_email IN " + str(to_del) + "; SELECT ROW_COUNT();"
				request       = "DELETE FROM ban WHERE target_email IN " + str(to_del)
				self.__symsp.request(request)
				rvl = self.__symsp.request ("SELECT ROW_COUNT()")
				self.__symsp.commit()
				return rvl[0][0]
			else:
				return 0
		else:
			return 0
		
	def banErrors(self):
		'''Ban the tuples found in self.__error_tuples and return the numbers of banned tuples: (temp,perm)'''	
		
		if 'temporary' in self.__error_tuples:
			t_error_tuples = self.__error_tuples['temporary']
		else:
			t_error_tuples = []

		if 'permanent' in self.__error_tuples:
			p_error_tuples = self.__error_tuples['permanent']
		else:
			p_error_tuples = []
			
		temp = 0
		perm = 0
		bl   = self.__bl
		
		for r in t_error_tuples:
			for l in t_error_tuples[r]:
				for u in t_error_tuples[r][l]:
					temp += bl.banTemp(r,l,u)
		for r in p_error_tuples:
			for l in p_error_tuples[r]:
				for u in p_error_tuples[r][l]:
					perm += bl.banPerm(r,l,u)
		
		return (temp, perm)
