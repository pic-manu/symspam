#! /usr/bin/python3
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

from symspamql import Mysql
from symspamdate import Date
from banmgr import PERMANENT_ERRORS,TEMPORARY_ERRORS
from erreur import PicErreur

#
# ErrStats = This class is useful to compute statistics about the errors found in the sympa logfile
#
class ErrStats(object):
	
	__symsp = None
	__sympa = None

	def __init__(self,sympa,symspam):
		self.__symsp = symspam
		self.__sympa = sympa
		
		if not sympa.isSympa() or not sympa.isReadonly():
			raise PicErreur("ERREUR INTERNE - sympa n'est pas une BD sympa RO")
		if symspam.isSympa():
			raise PicErreur("ERREUR INTERNE - symspam est une BD sympa - devrait être symspam")

	def addErrStats(self,date):
		'''Add the number of errors at date for each (robot, liste) tuple
		   date is a Date object (see symspamdate.py)'''

		ts_beg = date.getTimestamp()
		ts_end = date.getOtherDay(1).getTimestamp()
		iso_dat= date.getIsoformat()
		
		request = "SELECT robot_logs, list_logs, error_type_logs, status_logs \
					FROM `logs_table` \
					WHERE ((daemon_logs = 'bounced' AND status_logs = 'error') OR (daemon_logs = 'sympa_msg' AND status_logs = 'success')) AND date_logs >= %s AND date_logs < %s"
		
		val = self.__sympa.request(request,(ts_beg,ts_end))

		# Reformat information
		stats = {}
		for l in val:
			robot = l[0]
			liste = l[1]
			error = l[2]
			status= l[3]
			
			if stats.get(robot) == None:
				stats[robot] = {}
				
			r_stats = stats[robot]
			
			if r_stats.get(liste) == None:
				r_stats[liste] = {}
					
			l_stats = r_stats[liste]
			
			if l_stats == {}:
				l_stats['temporary_errors'] = 0
				l_stats['permanent_errors'] = 0
				l_stats['ignored_errors']   = 0
				l_stats['success']          = 0

			if status == 'error':
				if error in TEMPORARY_ERRORS:
					l_stats['temporary_errors'] += 1
				
				elif error in PERMANENT_ERRORS:
					l_stats['permanent_errors'] += 1
				
				else:
					l_stats['ignored_errors']   += 1
			else:
				l_stats['success'] += 1

		# Write info to the table errstat
		for r in stats:
			for l in stats[r]:
				l_stats = stats[r][l]
				tmp_err = l_stats['temporary_errors'] 
				prm_err = l_stats['permanent_errors']
				ign_err = l_stats['ignored_errors']
				success = l_stats['success']
				
				request = "INSERT INTO `errstats` \
							(robot,liste,date,temporary_errors,permanent_errors,ignored_errors,success) VALUES \
							(%s,%s,%s,%s,%s,%s,%s)"
				self.__symsp.request(request,(r,l,iso_dat,tmp_err,prm_err,ign_err,success))
		self.__symsp.commit()

	def addBanStats(self,banned,unbanned,date):
		'''Adds a line in the banstats table
		   banned, unbanned = number of tuples banned/unbanned at this time
		'''
		
		date_iso = date.getIsoformat()
		rvl       = self.__symsp.request("SELECT count(*) FROM `ban` WHERE `flag` = 1")
		rem_banned= rvl[0][0]
		rvl       = self.__symsp.request("SELECT count(*) FROM `ban` WHERE `flag` = 0")
		rem_unbanned= rvl[0][0]
		self.__symsp.request("INSERT INTO `banstats` (date, just_banned, just_unbanned, rem_banned, rem_unbanned) \
								VALUES (%s, %s, %s, %s, %s)",(date_iso,banned,unbanned,rem_banned,rem_unbanned))
		self.__symsp.commit()

