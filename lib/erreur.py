#! /usr/bin/python
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE erreur =  Definition de l'objet Erreur
#

class PicErreur(Exception):
	def __init__(self,msg,err=0):
		self.__msg = msg
		self.__err = err
	def getMsg(self):
		return self.__msg
	def getErr(self):
		return self.__err



