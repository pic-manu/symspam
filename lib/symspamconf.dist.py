#! /usr/bin/python
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE sympspamconf = Definition de la fonction getConf()

version = '0.1'


CONFIG  = {
		"SYMPARW_DB"     : "sympa",
		"SYMPARW_DBPASS" : "PASSWD",
		"SYMPARW_DBUSER" : "sympa",
		"SYMPARW_DBHOST" : "localhost"

		"SYMPARO_DB"     : "sympa",
		"SYMPARO_DBPASS" : "PASSWD",
		"SYMPARO_DBUSER" : "symparo",
		"SYMPARO_DBHOST" : "localhost"

		"SYMSPAM_DB"     : "symspam",
		"SYMSPAM_DBPASS" : "PASSWD",
		"SYMSPAM_DBUSER" : "symspam",
		"SYMSPAM_DBHOST" : "localhost"
}

def getConf(prm):
	'''renvoie la valeur du parametre de config passe en parametres, ou None'''
	try:
		return CONFIG[prm]

	except:
		return None



# TEST
if __name__ == '__main__':
	print(getConf('DB'))
	print(getConf('NAWAK'))

