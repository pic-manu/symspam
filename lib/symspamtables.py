#! /usr/bin/python3
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

from symspamql import Mysql
from symspamdate import Date
from erreur import PicErreur

VERSION = '1.1.O'

#
# SymSpamTables = This class creates or updates the tables of the application
#

# A companion function
def getMajv(v):
	'''Return the major version ie for 1.2.3 return 1.2'''
	v1 = v.split('.')
	v1.pop()
	v1 = '.'.join(v1)
	return v1
		
			
class SymSpamTables(object):

	def __getDbVersion(self):
		'''Return the version number stored in the database'''
		
		rvl = self.__symsp.request("SELECT `value` FROM `prms` WHERE `name`='VERSION'")
		return rvl[0][0]

	def __init__(self,symspam,date=None):
		'''If date is a Date object, update the DATE parameter !'''
		self.__symsp = symspam
		if symspam.isSympa() == True:
			raise("ERREUR INTERNE")

		# Detect the version of current database, if any
		db_version = ''
		try:
			db_version = self.__getDbVersion()
		except:
			# Version not found --> create the tables
			self.__createTables(date)
			
		# Same version as in db: OK
		if db_version == VERSION:
			return
			
		# if same major version, just update the version number in the database
		MVERSION    = getMajv(VERSION)
		Mdb_version = getMajv(db_version)
		
		if MVERSION == Mdb_version:
			self.__createTables(date)
			return
		else:
			if MVERSION == '1.1' and Mdb_version == '1.0':
				self.__updateTable_1_0_to_1_1()
			
		
		
	# -----------------------------------
	# Create and update tables when we install a new revision
	# -----------------------------------
	def __createTables(self,date):
		'''If necessary, create the tables needed by the application - called by the constructor'''

		try:
			self.__symsp.request("CREATE TABLE `prms` (\
									name VARCHAR(20) NOT NULL, \
									value VARCHAR(20) NULL)")
			self.__symsp.request("INSERT INTO prms \
									(name,value) VALUES ('VERSION',%s)", (VERSION,))
			if (isinstance(date,Date)):
				self.__symsp.request("INSERT INTO prms \
										(name,value) VALUES ('DATE',%s)", (date.getIsoformat(),))
		except:
			self.__symsp.request("UPDATE prms SET `value`=%s WHERE `name`='VERSION'", (VERSION,))
			if (isinstance(date,Date)):
				self.__symsp.request("UPDATE prms SET `value`=%s WHERE `name`='DATE'", (date.getIsoformat(),))

		self.__symsp.commit()
		
		try:
			self.__symsp.request("CREATE TABLE `ban` (\
									robot VARCHAR(80) NOT NULL, \
									list VARCHAR(50) NOT NULL, \
									target_email VARCHAR(100), \
									date INT(11), \
									start DATE NULL, \
									end DATE NULL, \
									duration INT(11), \
									flag INT(1))")
			self.__symsp.request("ALTER TABLE `ban` ADD UNIQUE( `robot`, `liste`, `target_email`, `date`)")

		except:
			pass

		try:
			self.__symsp.request("CREATE TABLE `errstats` (\
									robot VARCHAR(80) NOT NULL, \
									liste VARCHAR(50) NOT NULL, \
									date DATE NOT NULL, \
									temporary_errors INT(11) DEFAULT 0, \
									permanent_errors INT(11) DEFAULT 0, \
									ignored_errors INT(11) DEFAULT 0, \
									success INT(11) DEFAULT 0)")
			self.__symsp.request("ALTER TABLE `errstats` ADD UNIQUE( `robot`, `liste`, `date`)")
		except:
			pass

		try:
			self.__symsp.request("CREATE TABLE `banstats` (\
									date DATE NOT NULL, \
									just_banned INT(11) DEFAULT 0, \
									just_unbanned INT(11) DEFAULT 0, \
									rem_banned INT(11) DEFAULT 0, \
									rem_unbanned INT(11) DEFAULT 0)")
			self.__symsp.request("ALTER TABLE `errstats` ADD UNIQUE( `robot`, `liste`, `date`)")
		except:
			pass

		try:
			self.__symsp.request("CREATE TABLE `whitelist`(\
									date DATE NOT NULL, \
									counter INT(11) DEFAULT 0, \
									target_email VARCHAR(100))")
			self.__symsp.request("ALTER TABLE `whitelist` ADD UNIQUE( `date`, `target_email`)")
		except:
			pass

	# Updating tables from 1.0.x to 1.1.x
	def __updateTable_1_0_to_1_1(self):
		
		# Creating two new columns in ban table
		try:
			self.__symsp.request("ALTER TABLE `ban` ADD `start` DATE NULL AFTER `date`, ADD `end` DATE NULL AFTER `start`;")
			self.__symsp.request("ALTER TABLE `ban` ADD UNIQUE( `robot`, `list`, `target_email`);")
		except:
			pass

		self.__symsp.request("UPDATE `prms` SET `value`=%s WHERE `name`=%s",(VERSION,'VERSION'))
		self.__symsp.commit()

		# Populating those columns
		# - start = same date as the stamp of date (but in a more convenient format)
		# - end   = start + duration
		request  = "SELECT * FROM `ban` "
		b_tuples = self.__symsp.request(request)
		for b in b_tuples:
			start = Date(int(b[3]))
			end   = start.getOtherDay(int(b[6]))
			self.__symsp.request("UPDATE ban \
								  SET start=%s, end=%s \
								  WHERE robot=%s AND list=%s AND target_email=%s",
								 (start.getIsoformat(),end.getIsoformat(),b[0],b[1],b[2]))
		
		self.__symsp.commit()

