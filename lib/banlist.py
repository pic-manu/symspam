#! /usr/bin/python3
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

from symspamql import Mysql
from symspamdate import Date
from erreur import PicErreur

INITIAL_DURATION = 7 	# Initial ban duration, in days
MULT_DURATION    = 2	# mult factor for a duration in case of several bans
MAX_DURATION	 = 28   # Max duration of a ban (in days)
DAY				 = 3600 * 24	# How many seconds in a day ?
SUSP_DURATION    = 7 * DAY		# The suspend duration, in seconds

#
# BanList = This class maintains the list of banned tuples (robot, list, address)
#
class BanList(object):
	
	__symsp      = None
	__sympa      = None
	__white_list = None

	def __init__(self,sympa,symspam):
		self.__symsp = symspam
		self.__sympa = sympa
		
		if not sympa.isSympa():
			raise PicErreur("ERREUR INTERNE - sympa n'est pas une BD sympa")
		if symspam.isSympa():
			raise PicErreur("ERREUR INTERNE - symspam est une BD sympa - devrait être symspam")
			
	# -----------------------------------
	# Private, lowlevel methods
	# -----------------------------------

	def __suspendForAPeriod(self, robot, liste, email):
		'''Suspend from today for a period (SUSP_DURATION), even if already suspended - Return 0 if the address is not in the subscriber_table'''
		
		start = Date().getTimestamp()
		end   = start + SUSP_DURATION
		
		self.__sympa.request("UPDATE subscriber_table \
							  SET suspend_start_date_subscriber=%s, suspend_end_date_subscriber=%s, suspend_subscriber=%s \
							  WHERE robot_subscriber=%s AND list_subscriber=%s AND user_subscriber=%s",(start,end,1,robot,liste,email))
		rvl = self.__sympa.request ("SELECT ROW_COUNT()")
		self.__sympa.commit()
		return rvl[0][0]

	def __resume(self, robot, liste, email):
		'''Resume the subscription, raise an exception if not subscribed
			EDIT - Do not do anything, the subscription will be automatically resumed in a few days !!!
		'''
		return
		
		if self.isSubscribed(robot,liste,email):
			self.__sympa.request("UPDATE subscriber_table \
								  SET suspend_start_date_subscriber=%s, suspend_end_date_subscriber=%s, suspend_subscriber=%s \
								  WHERE robot_subscriber=%s AND list_subscriber=%s AND user_subscriber=%s",(0,0,0,robot,liste,email))
		else:
			raise PicErreur ("ERREUR - " + email + " n'est plus abonné à " + liste + '@' + robot)
			
	# -----------------------------------
	# Public, highlevel methods
	# -----------------------------------
	def isSuspended(self, robot, liste, email):
		'''Returns True if the tuple is already suspended, or is it is not found
		   Returns False if the tuple is found in the sympa db, and not suspended'''
		
		is_susp = self.__sympa.request("SELECT suspend_subscriber FROM `subscriber_table` \
										WHERE `robot_subscriber` = %s AND `list_subscriber` = %s AND `user_subscriber` = %s",(robot,liste,email))
		
		if len(is_susp) == 0:
			return True
		else:
			return (is_susp[0][0] == 1)

	def isSubscribed(self, robot, liste, email):
		'''Returns True if the user is in the subscriber table, False if not'''
		
		is_susp = self.__sympa.request("SELECT suspend_subscriber FROM `subscriber_table` \
										WHERE `robot_subscriber` = %s AND `list_subscriber` = %s AND `user_subscriber` = %s",(robot,liste,email))

		if len(is_susp) == 0:
			return False
		else:
			return True
			
	def isBanned(self, robot, liste, email):
		'''return True if the user is in the ban table  AND if flag = 1
		   return False if the user is not in the table, of if flag = 0'''
		
		is_in_table = self.__symsp.request("SELECT `flag` FROM `ban` WHERE \
												`robot` = %s AND \
												`list` = %s AND \
												`target_email` = %s",(robot,liste,email))
		if len(is_in_table) == 0:
			return False
		else:
			if is_in_table[0][0] == 1:
				return True
			else:
				return False

	def isKnown(self, robot, liste, email):
		'''return True if the user is in the ban table, False if he is not'''
		
		is_in_table = self.__symsp.request("SELECT `flag` FROM `ban` WHERE \
												`robot` = %s AND \
												`list` = %s AND \
												`target_email` = %s",(robot,liste,email))
		if len(is_in_table) == 0:
			return False
		else:
			return True

	def unbanPerm(self,robot,liste,email):
		'''Permanently unban user, ie remove from the table ban
		   Return 1 if user deleted from the table, 0 if he was not in the table'''

		if not self.isKnown(robot,liste,email):
			return 0

		# resume ne fait RIEN
		self.__resume(robot,liste,email)
		
		self.__symsp.request("DELETE FROM ban \
								WHERE robot = %s AND \
								list        = %s AND \
								target_email  = %s",(robot, liste, email))
		self.__symsp.commit()

		return 1


	def unbanTemp(self,robot,liste,email):
		'''Temporary unban user, ie set flag to 0
		   set start to the today's date and end to NULL
		   return 1 (unbanned) or 0 (was already unbanned)'''
		
		if not self.isBanned(robot,liste,email):
			return 0

		self.__resume(robot,liste,email)
		start = Date()
		self.__symsp.request("UPDATE ban \
							  SET flag=0, start=%s, end=NULL \
							  WHERE robot=%s AND list=%s AND target_email=%s",(start.getIsoformat(),robot,liste,email))
		self.__symsp.commit()
		
		return 1
		
	def getBanDuration(self,robot,liste,email):
		'''Get the ban duration, raise an exception if not known
		   If banned but known, return the current ban duration
		   If not banned but known, return the opposite (ban duration = 7 but not banned, retrun -7)
		'''
		
		rvl = self.__symsp.request("SELECT `flag`,`duration` FROM `ban` WHERE \
										`robot` = %s AND \
										`list` = %s AND \
										`target_email` = %s",(robot,liste,email))

		if rvl == []:
			raise PicErreur("ERREUR - " + robot + " " + liste + " " + email + " n'est pas connu")

		elif len(rvl) > 1:
			raise PicErreur("ERREUR INTERNE - " + str(len(rvl)) + " lignes dans la réponse")

		else:
			if rvl[0][0] == 1:
				return rvl[0][1]
			else:
				return -rvl[0][1]

	def getSuspendEndDate(self,robot,liste,email):
		'''If suspended return the end date. If not suspended return 0
		   If not subscribed raise an exception'''
		rvl = self.__sympa.request("SELECT `suspend_subscriber`, `suspend_end_date_subscriber` FROM `subscriber_table` WHERE \
										`robot_subscriber` = %s AND \
										`list_subscriber` = %s AND \
										`user_subscriber` = %s",(robot,liste,email))
										
		if rvl == []:
			raise PicErreur("ERREUR INTERNE - ")
		else:
			if rvl[0][0] == 1:
				if rvl[0][1] == None:
					return 0
				else:
					return rvl[0][1]
			else:
				return 0

	def banTemp(self,robot,liste,email):
		'''Ban this user for a limited period. Return 1 if banned, 0 if he is already banned'''

		if self.isBanned(robot,liste,email):
			return 0
			
		today     = Date()
		today_ts  = today.getTimestamp()
	
		self.__suspendForAPeriod(robot, liste, email)
		
		if not self.isKnown(robot,liste,email):
			duration  = INITIAL_DURATION
			start     = Date(today_ts)
			end       = start.getOtherDay(duration)
			self.__symsp.request("INSERT INTO ban \
									(robot,list,target_email,date,start,end,duration,flag) \
									VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
									(robot,liste,email,today_ts,start.getIsoformat(),end.getIsoformat(),duration,1))
			return 1
		
		# This user was known, but not banned
		else:
			duration = self.getBanDuration(robot, liste, email)
			if abs(duration * MULT_DURATION) <= MAX_DURATION:
				duration = abs(duration * MULT_DURATION)
			else:
				duration = abs(duration)

			start     = today
			end       = start.getOtherDay(duration)
				
			self.__symsp.request("UPDATE ban \
								  SET date=%s, start=%s, end=%s, duration=%s,flag=%s \
								  WHERE robot=%s AND list=%s AND target_email=%s",
								 (today_ts,start.getIsoformat(),end.getIsoformat(),duration,1,robot,liste,email))
			return 1

	def banPerm(self,robot,liste,email):
		'''Permanently ban this user. Return 1 if banned, 0 if he is already banned'''

		# If already banned, do not ban again !
		if self.isBanned(robot, liste, email):
			return 0
					
		today     = Date()
		today_ts  = today.getTimestamp()
		duration  = 10000
		start     = Date(today_ts)
		end       = start.getOtherDay(duration)

		self.__suspendForAPeriod(robot, liste, email)

		if not self.isKnown(robot,liste,email):
			self.__symsp.request("INSERT INTO ban \
									(robot,list,target_email,date,start,end,duration,flag) \
									VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
									(robot,liste,email,today_ts,start.getIsoformat(),end.getIsoformat(),duration,1))
			return 1
		
		# This user is known
		else:
			self.__symsp.request("UPDATE ban \
								  SET date=%s, start=%s, end=%s, duration=%s,flag=%s \
								  WHERE robot=%s AND list=%s AND target_email=%s",
								 (today_ts,start.getIsoformat(),end.getIsoformat(),duration,1,robot,liste,email))
			return 1

	def synchro(self):
		'''For each line in symspam, extend the suspend duration if necessary, or unban the tuple
		   Return the number of unbanned tuples
		'''
		
		banned = self.__symsp.request("SELECT * FROM `ban`")
		today    = Date()
		today_ts = today.getTimestamp()
		un       = 0

		for l in banned:
			(robot,liste,email,date,start,end,duration,flag) = l

			# The user is still in the subscriber table
			if self.isSubscribed(robot, liste, email):
	
				# The tuple is permanently banned
				if flag==1 and duration==10000:
					self.__suspendForAPeriod(robot,liste,email)
					continue

				# The tuple is temporarily banned...
				if flag==1 and duration>0:
					if round((today_ts - date)/DAY) < duration:
						self.__suspendForAPeriod(robot,liste,email)
					else:
						un += self.unbanTemp(robot,liste,email)
					continue
			else:
				un += self.unbanPerm(robot,liste,email)

		return un
