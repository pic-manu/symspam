#! /usr/bin/python
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

from symspamconf import getConf
from symspamql import *
from symspamdate import Date
from banlist import BanList,DAY
from symspamtables import SymSpamTables,getMajv
from errstats import ErrStats
from banmgr import BanMgr

from erreur import PicErreur
import datetime

import unittest

def create_tables():
	'''Create the tables inside the test databases'''

	_symsp = Mysql('SYMSPAM')
	st = SymSpamTables(_symsp)

	try:
		_symsp.request("DROP TABLE essai,essai2")
	except mysql.connector.errors.ProgrammingError as e:
		pass
		
	_symsp.request("CREATE TABLE essai ( entiers INT PRIMARY KEY NOT NULL )")
	_symsp.request("CREATE TABLE essai2 ( A INT PRIMARY KEY NOT NULL, B INT NOT NULL DEFAULT 0, C INT NOT NULL DEFAULT 0, D INT NOT NULL DEFAULT 0, E INT NOT NULL DEFAULT 0 )")
		
	# Warning ! Do not use the production tables !
	_sympa = Mysql('SYMPARW')
	
	try:
		_sympa.request("DROP TABLE subscriber_table,essai,logs_table")
	except mysql.connector.errors.ProgrammingError as e:
		pass
		
	_sympa.request("CREATE TABLE `subscriber_table` (\
						list_subscriber VARCHAR(50) NULL, \
						user_subscriber VARCHAR(100) NULL, \
						robot_subscriber VARCHAR(80) NULL, \
						suspend_end_date_subscriber INT(11) NULL, \
						suspend_start_date_subscriber INT(11) NULL, \
						suspend_subscriber INT(1) NULL)")

	_sympa.request("CREATE TABLE `logs_table` (\
					daemon_logs VARCHAR(50) NOT NULL, \
					date_logs INT(11), \
					error_type_logs VARCHAR(150) NULL, \
					list_logs VARCHAR(50) NULL, \
					robot_logs VARCHAR(80) NULL, \
					status_logs VARCHAR(10) NOT NULL, \
					target_email_logs VARCHAR(100) NULL, \
					user_email_logs VARCHAR(100) NULL )")

#@unittest.skip("bouh")
class TestSymspamTables(unittest.TestCase):
	'''Testing some methods of SymspamTables'''
	
	def test_majv(self):
		
		self.assertEqual(getMajv('1.0.3'),'1.0')
		self.assertEqual(getMajv('1.1.5'),'1.1')
		
#@unittest.skip("bouh")
class TestMysql(unittest.TestCase):
	'''Testing low level Mysql class'''
	
	def test_request(self):
		'''Testing sql requests in the SYMSPAM database'''
		
		# Do not use any stupid prefix
		self.assertRaises(PicErreur,Mysql,"STUPID-PREFIX")
			
		_symsp = Mysql("SYMSPAM")

		self.assertEqual (_symsp.isReadonly(),False)
		self.assertEqual (_symsp.isSympa(),False)
		
		# Testing a table with 1 column
		_symsp.request("INSERT INTO essai (entiers) VALUES (4),(5),(6)")
		rvl = _symsp.request("SELECT ROW_COUNT()")
		_symsp.commit()
		self.assertEqual(rvl[0][0],3)

		values = _symsp.request("SELECT * FROM essai") 
		self.assertEqual(values,[(4,),(5,),(6,)])
		
		values = _symsp.request("SELECT * FROM essai") 
		self.assertEqual(values,[(4,),(5,),(6,)])
		
		# Primary keys cannot be duplicated
		self.assertRaises(mysql.connector.errors.IntegrityError,_symsp.request,"INSERT INTO essai (entiers) VALUES (4),(5),(6)")
		
		# Testing a table with 2 columns
		_symsp.request("INSERT INTO essai2 (A,B,C,D) VALUES (4,40,400,4000)")
		_symsp.request("INSERT INTO essai2 (A,B,C,D) VALUES (5,50,500,5000)")
		_symsp.request("INSERT INTO essai2 (A,B,C,D) VALUES (6,60,600,6000)")
		values = _symsp.request("SELECT * FROM essai2")

		# It works without commit: why ?
		self.assertEqual(values,[(4,40,400,4000,0),(5,50,500,5000,0),(6,60,600,6000,0)])
		
		# Testing a parameter
		values = _symsp.request("SELECT * FROM essai2 WHERE A > %s AND A < %s",(4,6))
		self.assertEqual(values,[(5,50,500,5000,0)])
		
		# Testing an SQL error
		self.assertRaises(mysql.connector.errors.ProgrammingError,_symsp.request,"SELECT * FROM essai3")

		# Request returning nothing
		values = _symsp.request("SELECT * FROM essai2 WHERE A < %s",(-666,))
		self.assertEqual(values,[])
		
		# Deleting something wich does not exist
		_symsp.request("DELETE FROM essai2 WHERE A > 10000000")
		
	def test_symparo(self):
		'''Testing the read only connection to sympa'''
	
		_symsp = Mysql("SYMPARO")

		self.assertEqual (_symsp.isReadonly(),True)
		self.assertEqual (_symsp.isSympa(),True)
		
		# should not be able to create a table
		self.assertRaises(mysql.connector.errors.ProgrammingError,_symsp.request,"CREATE TABLE essai ( entiers INT PRIMARY KEY NOT NULL )")

	def test_symparw(self):
		'''Testing the ReadWrite connection to sympa'''
		
		_symsp = Mysql("SYMPARW")
		
		self.assertEqual (_symsp.isReadonly(),False)
		self.assertEqual (_symsp.isSympa(),True)
		
		# should be able to create/drop a table
		rvl = _symsp.request("CREATE TABLE essai ( entiers INT PRIMARY KEY NOT NULL )")
		self.assertEqual(rvl,None)

		rvl = _symsp.request("DROP TABLE essai")
		self.assertEqual(rvl,None)
		
#@unittest.skip("bouh")
class TestDate(unittest.TestCase):
	'''Testing the Date object'''
	
	def test_ctor(self):

		d1 = Date('2020-01-01')
		self.assertEqual(d1._Date__date.timestamp(),1577833200)

		d1b= Date('2020-1-1')
		self.assertEqual(d1b._Date__date.timestamp(),1577833200)
		
		d2 = Date(1577833300)
		self.assertEqual(d2._Date__date.timestamp(),1577833200)

		d3 = Date(d2._Date__date)
		self.assertEqual(d3._Date__date.timestamp(),1577833200)

		d4 = Date(datetime.date(2020,1,1))
		self.assertEqual(d4._Date__date.timestamp(),1577833200)

		d5 = Date()
		self.assertGreater(d5._Date__date.timestamp(),1577833200)
		
		self.assertRaises(PicErreur,Date,'toto')
		self.assertRaises(PicErreur,Date,'4.5')
		self.assertRaises(PicErreur,Date,'2020-20-20')
		
	def test_getter(self):
		d = Date('2020-01-01')
		self.assertEqual('2020-01-01',d.getIsoformat())
		self.assertEqual(1577833200,d.getTimestamp())
		self.assertEqual('2020-01-06',d.getOtherDay(5).getIsoformat())
		self.assertEqual('2019-12-25',d.getOtherDay(-7).getIsoformat())
		self.assertEqual(d.getFormatted('%m %d'),'01 01')
		self.assertEqual(d.getFormatted('%b %d'),'Jan 01')
		
	def test_compute(self):	
		d1 = Date('2020-03-14')
		d2 = Date('2020-03-21')
		d3 = Date('2020-04-11')
		self.assertEqual(round((d2.getTimestamp()-d1.getTimestamp())/DAY),7)
		self.assertEqual(round((d3.getTimestamp()-d1.getTimestamp())/DAY),28)
		self.assertEqual(round((d3.getTimestamp()-d2.getTimestamp())/DAY),21)

#@unittest.skip("bouh")
class TestBanList2(unittest.TestCase):
	'''Testing isBanned and isKnown'''
	
	def setUp(self):
		_symsp = Mysql("SYMSPAM")
		_symsp.request("TRUNCATE TABLE ban")

		# a1 is banned, a2 is not banned yet (but was), a3 not banned at all
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a1@a.fr',100,7,1)")
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a2@a.fr',100,7,0)")
		_symsp.commit()

	def test1(self):
		'''testing isKnown and isBanned'''
		sympa = Mysql("SYMPARO")
		mysql = Mysql("SYMSPAM")
		bl = BanList(sympa,mysql)
		self.assertEqual(bl.isKnown('robot1','liste1','a1@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a1@a.fr'),True)
		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isKnown('robot1','liste1','a3@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),False)

		self.assertEqual(bl.isKnown('robot2','liste1','a1@a.fr'),False)
		self.assertEqual(bl.isKnown('robot1','liste2','a1@a.fr'),False)

class TestBanList3(unittest.TestCase):
	'''Testing isSuspended, __suspendForAPeriod, getSuspendEndDate and unbanPerm'''

	def setUp(self):
		_symsp = Mysql("SYMSPAM")
		_symsp.request("TRUNCATE TABLE ban")

		_sympa = Mysql("SYMPARW")
		
		# WARNING ! Do not test with the production database !
		_sympa.request("TRUNCATE TABLE subscriber_table")

		# a1 is banned, a2 is not banned yet (but was), a3 not banned at all
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a1@a.fr',100,7,1)")
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a2@a.fr',100,7,0)")
		_symsp.commit()
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber,suspend_subscriber) \
								VALUES ( 'liste1','a1@a.fr','robot1',1)")
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber,suspend_subscriber) \
								VALUES ( 'liste1','a2@a.fr','robot1',0)")
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber,suspend_subscriber) \
								VALUES ( 'liste1','a3@a.fr','robot1',0)")
		_sympa.commit()

	def test03(self):
		'''Testing isSuspended, isSubscribed'''
		
		_sympa = Mysql("SYMPARO")
		_symsp = Mysql("SYMSPAM")
		bl    = BanList(_sympa,_symsp)

		self.assertEqual(bl.isSuspended('robot1','liste1','a1@a.fr'),True)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isSuspended('robot1','liste1','a3@a.fr'),False)
		self.assertEqual(bl.isSuspended('robot1','liste1','a4@a.fr'),True)

		self.assertEqual(bl.isSubscribed('robot1','liste1','a1@a.fr'),True)
		self.assertEqual(bl.isSubscribed('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isSubscribed('robot1','liste1','a3@a.fr'),True)
		self.assertEqual(bl.isSubscribed('robot1','liste1','a4@a.fr'),False)

	def test10(self):
		'''Testing __suspendForAPeriod, getSuspendEndDate, __resume'''
		
		_sympa = Mysql("SYMPARW")
		_symsp = Mysql("SYMSPAM")
		bl = BanList(_sympa,_symsp)
		
		cnt = bl._BanList__suspendForAPeriod('robot1','liste1','a2@a.fr')
		self.assertEqual(cnt,1)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.getSuspendEndDate('robot1','liste1','a2@a.fr'),Date().getTimestamp() + 7 * DAY)
		
		bl._BanList__suspendForAPeriod('robot2','liste2','a5@a.fr')
		self.assertEqual(bl.isSuspended('robot2','liste2','a5@a.fr'),True)
		self.assertRaises(PicErreur,bl.getSuspendEndDate,'robot1','liste1','a5@a.fr')
		
		# __resume makes nothing now - Still suspended
		bl._BanList__resume('robot1','liste1','a2@a.fr')
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),True)
		self.assertNotEqual(bl.getSuspendEndDate('robot1','liste1','a2@a.fr'),0)
		
		#self.assertRaises(PicErreur, bl._BanList__resume, 'robot2','liste2','a5@a.fr')
		self.assertEqual(bl.isSuspended('robot2','liste2','a5@a.fr'),True)
		
	def test20(self):
		'''Testing unbanPerm'''

		# Does not work in readonly mode
		# Now it works in readonly mode because __resume makes nothing !
		_sympa = Mysql("SYMPARO")
		_symsp = Mysql("SYMSPAM")
		#bl0 = BanList(_sympa,_symsp)
		#self.assertRaises(mysql.connector.errors.ProgrammingError,bl0.unbanPerm,'robot1','liste1','a1@a.fr')

		# NOT readonly !
		#_sympa = Mysql("SYMPARW")
		bl = BanList(_sympa,_symsp)
		
		# Unban a1@a.fr (was really banned)
		self.assertEqual(bl.isBanned('robot1','liste1','a1@a.fr'),True)
		
		unbanned = bl.unbanPerm('robot1','liste1','a1@a.fr')
		self.assertEqual(unbanned,1)

		# Will receive the mails now
		# NO ! __resume makes nothing, still resumed
		#self.assertEqual(bl.isSuspended('robot1','liste1','a1@a.fr'),False)
		self.assertEqual(bl.isSuspended('robot1','liste1','a1@a.fr'),True)

		# Removed from the table !
		self.assertEqual(bl.isKnown('robot1','liste1','a1@a.fr'),False)
		
		# If unbanned again, we return 0
		self.assertEqual(bl.unbanPerm('robot1','liste1','a1@a.fr'),0)
		
		# Unban a2@a.fr (was in the table, but not banned yet)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),False)

		# Now completely unbanned !
		self.assertEqual(bl.unbanPerm('robot1','liste1','a2@a.fr'),1)
		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),False)

		# Try to unban it again = returns 0
		self.assertEqual(bl.unbanPerm('robot1','liste1','a2@a.fr'),0)
		
		# Ban a3 = returns 0 too
		self.assertEqual(bl.unbanPerm('robot1','liste1','a3@a.fr'),0)
		
	def test30(self):
		'''Testing unbanTemp'''

		# NOT readonly !		
		_sympa = Mysql("SYMPARW")
		_symsp = Mysql("SYMSPAM")
		bl = BanList(_sympa,_symsp)
		
		self.assertEqual(bl.isBanned('robot1','liste1','a1@a.fr'),True)
		self.assertEqual(bl.unbanTemp('robot1','liste1','a1@a.fr'),1)
		self.assertEqual(bl.isBanned('robot1','liste1','a1@a.fr'),False)
		self.assertEqual(bl.isKnown('robot1','liste1','a1@a.fr'),True)
		self.assertEqual(bl.unbanTemp('robot1','liste1','a1@a.fr'),0)
		self.assertEqual(bl.unbanTemp('robot1','liste1','a2@a.fr'),0)
		self.assertEqual(bl.unbanTemp('robot1','liste1','a5@a.fr'),0)

	def test40(self):
		'''Testing banTemp and getBanDuration'''
		
		# NOT readonly !
		_sympa = Mysql("SYMPARW")
		_symsp = Mysql("SYMSPAM")
		bl = BanList(_sympa,_symsp)
		
		# Ban a3 (is not in ban)
		self.assertRaises(PicErreur,bl.getBanDuration,'robot1','liste1','a3@a.fr')

		self.assertEqual(bl.isKnown('robot1','liste1','a3@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),False)
		self.assertEqual(bl.isSuspended('robot1','liste1','a3@a.fr'),False)
		
		self.assertEqual(bl.banTemp('robot1','liste1','a3@a.fr'),1)
		self.assertEqual(bl.isKnown('robot1','liste1','a3@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),True)
		self.assertEqual(bl.isSuspended('robot1','liste1','a3@a.fr'),True)
		
		self.assertEqual(bl.banTemp('robot1','liste1','a3@a.fr'),0)

		self.assertEqual(bl.getBanDuration('robot1','liste1','a3@a.fr'),7)
		
		# Ban a2 (is in ban)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),-7)

		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),False)

		self.assertEqual(bl.banTemp('robot1','liste1','a2@a.fr'),1)
		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),True)
		
		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),14)
		self.assertEqual(bl.unbanTemp('robot1','liste1','a2@a.fr'),1)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),-14)

		self.assertEqual(bl.banTemp('robot1','liste1','a2@a.fr'),1)
		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),True)

		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),28)
		self.assertEqual(bl.unbanTemp('robot1','liste1','a2@a.fr'),1)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),-28)

		self.assertEqual(bl.banTemp('robot1','liste1','a2@a.fr'),1)
		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),True)

		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),28)
		self.assertEqual(bl.unbanTemp('robot1','liste1','a2@a.fr'),1)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),-28)
		
	def test50(self):
		'''Testing banPerm'''
		
		# NOT readonly !
		_sympa = Mysql("SYMPARW")
		_symsp = Mysql("SYMSPAM")
		bl = BanList(_sympa,_symsp)
		
		# Ban a3 (is not in ban)
		self.assertEqual(bl.isKnown('robot1','liste1','a3@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),False)
		self.assertEqual(bl.isSuspended('robot1','liste1','a3@a.fr'),False)
		
		self.assertEqual(bl.banPerm('robot1','liste1','a3@a.fr'),1)
		self.assertEqual(bl.isKnown('robot1','liste1','a3@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),True)
		self.assertEqual(bl.isSuspended('robot1','liste1','a3@a.fr'),True)

		self.assertEqual(bl.banPerm('robot1','liste1','a3@a.fr'),0)

		self.assertEqual(bl.getBanDuration('robot1','liste1','a3@a.fr'),10000)
		
		# Ban a2 (is in ban)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),-7)

		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),False)

		self.assertEqual(bl.banPerm('robot1','liste1','a2@a.fr'),1)
		self.assertEqual(bl.isKnown('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),True)
		self.assertEqual(bl.isSuspended('robot1','liste1','a2@a.fr'),True)
		
		self.assertEqual(bl.getBanDuration('robot1','liste1','a2@a.fr'),10000)

class TestBanList4(unittest.TestCase):
	'''Testing synchro'''

	def setUp(self):
		_symsp = Mysql("SYMSPAM")
		_symsp.request("TRUNCATE TABLE ban")

		_sympa = Mysql("SYMPARW")
		
		# WARNING ! Do not test with the production database !
		_sympa.request("TRUNCATE TABLE subscriber_table")

		base = Date().getTimestamp() - 28 * DAY
		d = []
		for i in list(range(28)):
			d.append(base + i * DAY)
		
		self.__d = d
			
		# a1 was banned for 28 days on d0: no more banned after synchro
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a1@a.fr',%s,28,1)",(d[0],))
		
		# a2 was banned for 28 days on d7: still banned after synchro
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a2@a.fr',%s,28,1)",(d[7],))

		# a3 was permanently banned on d21: still banned !
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a3@a.fr',%s,10000,1)",(d[21],))

		# a4 was banned on d14, but a4 was dropped by list owner = unbanned !
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a4@a.fr',%s,10000,1)",(d[14],))

		# a5 is not banned yet
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a5@a.fr',%s,7,0)",(d[14],))
		_symsp.commit()

		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber,suspend_subscriber) \
								VALUES ( 'liste1','a1@a.fr','robot1',1)")
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber,suspend_subscriber) \
								VALUES ( 'liste1','a2@a.fr','robot1',1)")
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber,suspend_subscriber) \
								VALUES ( 'liste1','a3@a.fr','robot1',1)")
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber,suspend_subscriber) \
								VALUES ( 'liste1','a5@a.fr','robot1',0)")
		_sympa.commit()

	def test01(self):
		_sympa = Mysql("SYMPARW")
		_symsp = Mysql("SYMSPAM")
		bl    = BanList(_sympa,_symsp)
		d7 = Date().getTimestamp() + 7 * DAY
		
		un = bl.synchro()
		
		self.assertEqual(bl.getSuspendEndDate('robot1','liste1','a1@a.fr'),0)
		self.assertEqual(bl.getSuspendEndDate('robot1','liste1','a2@a.fr'),d7)
		self.assertEqual(bl.getSuspendEndDate('robot1','liste1','a3@a.fr'),d7)
		self.assertRaises(PicErreur, bl.getSuspendEndDate,'robot1','liste1','a4@a.fr')
		self.assertEqual(bl.getSuspendEndDate('robot1','liste1','a5@a.fr'),0)
		self.assertEqual(un,2)
		

class TestErrStats(unittest.TestCase):
	'''Testing ErrStats object'''

	def setUp(self):
		_sympa = Mysql("SYMPARW")

		_sympa.request("TRUNCATE TABLE logs_table")

		dte1 = Date('2020-03-15')
		dte2 = Date('2020-03-25')
		
		# An ignored error
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'5.0.0','liste1','robot1','error','a1@a.fr')",
								(dte1.getTimestamp()+1000,))

		# A permanent error (2 times)
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a2@a.fr')",
								(dte1.getTimestamp()+1500,))
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a2@a.fr')",
								(dte1.getTimestamp()+2000,))

		# A permanent error (2 times, some other list)
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'5.1.1','liste2','robot1','error','a2@a.fr')",
								(dte1.getTimestamp()+1510,))
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'5.1.1','liste2','robot1','error','a2@a.fr')",
								(dte1.getTimestamp()+2010,))
		
		# Some succes
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,user_email_logs) VALUES ('sympa_msg',%s,'','liste1','robot1','success','b2@a.fr')",
								(dte1.getTimestamp()+1500,))
								
		# A temporary error
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'4.2.2','liste1','robot1','error','a3@a.fr')",
								(dte1.getTimestamp()+2500,))

		# Same errors later
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'5.0.0','liste1','robot1','error','a1@a.fr')",
								(dte2.getTimestamp()+1100,))
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a2@a.fr')",
								(dte2.getTimestamp()+1600,))
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a2@a.fr')",
								(dte2.getTimestamp()+2100,))
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) VALUES ('bounced',%s,'4.2.2','liste1','robot1','error','a3@a.fr')",
								(dte2.getTimestamp()+2600,))
		_sympa.commit()
		
		_symsp = Mysql("SYMSPAM")
		_symsp.request("TRUNCATE TABLE `ban`")
		_symsp.request("TRUNCATE TABLE `banstats`")
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a1@a.fr',%s,28,1)",(dte1.getTimestamp(),))
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a2@a.fr',%s,28,1)",(dte1.getTimestamp(),))
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a3@a.fr',%s,10000,1)",(dte2.getTimestamp(),))
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a4@a.fr',%s,10000,1)",(dte2.getTimestamp(),))
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
								VALUES ( 'robot1','liste1','a5@a.fr',%s,7,0)",(dte1.getTimestamp(),))
		_symsp.commit()
		
		_symsp.request("TRUNCATE TABLE `errstats`")
	
	def test1(self):
		'''Testing addErrStats'''
		
		_sympa = Mysql("SYMPARO")
		_symsp = Mysql("SYMSPAM")
		es     = ErrStats(_sympa,_symsp)

		# Adding stats for dte1
		date = Date('2020-03-15')
		es.addErrStats(date)
		
		# Already in the table 
		self.assertRaises(mysql.connector.errors.IntegrityError,es.addErrStats,date)

		# Use a new local variable for _symsp to avoid cache effects with mariadb !
		_symsp = None
		_symsp = Mysql("SYMSPAM")
		table   = _symsp.request("SELECT temporary_errors, permanent_errors, ignored_errors, success FROM `errstats`")
		shouldbe = [ (1,2,1,1),(0,2,0,0) ]
		self.assertEqual(table, shouldbe)
		_symsp = None
		
		# There was no mail at this date -> stats unchanged
		es.addErrStats(Date('2020-03-20'))
		_symsp = Mysql("SYMSPAM")
		table   = _symsp.request("SELECT temporary_errors, permanent_errors, ignored_errors, success FROM `errstats`")
		shoulbe = [ (1,2,1,1),(0,2,0,0) ]
		self.assertEqual(table, shoulbe)
		_symsp = None

		# Adding stats for dte2
		es.addErrStats(Date('2020-03-25'))
		_symsp = Mysql("SYMSPAM")
		table   = _symsp.request("SELECT temporary_errors, permanent_errors, ignored_errors, success FROM `errstats` ORDER BY `date`,`liste`")
		shouldbe = [ (1,2,1,1),(0,2,0,0),(1,2,1,0)]
		self.assertEqual(table, shouldbe)
		
		# What about the dates ?
		# 2 lines (2 lists) on 2020-03-15, no line on 2020-03-20, 1 line on 2020-03-25
		table   = _symsp.request("SELECT date FROM `errstats` ORDER BY `date`")
		table   = [t[0].isoformat() for t in table]
		self.assertEqual(table, ['2020-03-15','2020-03-15','2020-03-25'])

	def test2(self):
		'''Testing addBanStats'''

		_sympa = Mysql("SYMPARO")
		_symsp = Mysql("SYMSPAM")
		es     = ErrStats(_sympa,_symsp)
		_symsp = None	# CLose the databse to be sure it is saved !

		es.addBanStats(3,5,Date())
		
		_symsp = Mysql("SYMSPAM")

		[(date,just_banned,just_unbanned,rem_banned,rem_unbanned)] = _symsp.request("SELECT date,just_banned,just_unbanned,rem_banned,rem_unbanned FROM `banstats`")
		self.assertEqual(date.isoformat(),Date().getIsoformat())
		self.assertEqual(rem_banned,4)
		self.assertEqual(rem_unbanned,1)
		self.assertEqual(just_banned,3)
		self.assertEqual(just_unbanned,5)
		
#@unittest.skip("bouh")
class TestBanMgr1(unittest.TestCase):
	'''Testing white_list and __filterErrors methods'''
	
	def setUp(self):
		_sympa = Mysql("SYMPARW")
		_sympa.request("TRUNCATE TABLE logs_table")
		_symsp = Mysql("SYMSPAM")
		_symsp.request("TRUNCATE TABLE ban")
		_symsp.request("TRUNCATE TABLE whitelist")


		d  = []
		self.__d = d
		d0 = Date().getTimestamp()-10*DAY
		for i in range(0,10):
			d.append(Date(d0 + i*DAY))
		
		# An ignored error
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'5.0.0','liste1','robot1','error','a1@a.fr')",(d[1].getTimestamp(),))

		# A permanent error (2 times)
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a2@a.fr')",(d[2].getTimestamp(),))
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a2@a.fr')",(d[2].getTimestamp()+DAY/3,))
								
		# A temporary error
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'4.2.2','liste1','robot1','error','a3@a.fr')",(d[4].getTimestamp(),))

		# a4 is in the whitelist (see symspam_wl.py)
		#_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
        #                VALUES ('',%s,'','liste1','robot1','success','a4@a.fr')",(d[4].getTimestamp(),))
		#_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
        #                VALUES ('bounced',%s,'4.2.2','liste1','robot1','success','a4@a.fr')",(d[5].getTimestamp(),))
		_symsp.request("INSERT INTO `whitelist` (date,counter,target_email) \
							VALUES (%s,%s,%s)",(d[4].getIsoformat(),4,'a4@a.fr'))
		_symsp.request("INSERT INTO `whitelist` (date,counter,target_email) \
							VALUES (%s,%s,%s)",(d[5].getIsoformat(),3,'a4@a.fr'))

		# An error, later
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a5@a.fr')",(d[6].getTimestamp(),))

		_sympa.commit()
		_symsp.commit()
		
		
	def test1(self):
		'''Testing logs analysis, there is no white list'''
		d     = self.__d
		sympa = Mysql("SYMPARW")
		mysql = Mysql("SYMSPAM")
		l     = BanMgr(sympa,mysql,d[1],d[3])

		# There is no "white list", because we end the reading too early
		self.assertEqual(l._BanMgr__white_list,set())

		filtered = l._BanMgr__filterErrors()
		shouldbe = {'ignored'   : { 'robot1' : { 'liste1' : { 'a1@a.fr' : { '5.0.0' : 1 }} }}, 
					'permanent' : { 'robot1' : { 'liste1' : { 'a2@a.fr' : { '5.1.1' : 2 }} }},
					}
					#'temporary' : { 'robot1' : { 'liste1' : { 'a3@a.fr' : { '5.7.1' : 1 },
					#										  'a4@a.fr' : { '5.7.1' : 1 }} }}}
		self.assertEqual(filtered, shouldbe)

	#@unittest.skip("bouh")		
	def test2(self):
		'''Testing logs analysis, this time there is a white list'''
		d = self.__d
		sympa = Mysql("SYMPARW")
		mysql = Mysql("SYMSPAM")
		l     = BanMgr(sympa,mysql,d[1],d[5])
		
		# This time the 'success' will be seen and the error on a4@a.fr will not be reported
		self.assertEqual(l._BanMgr__white_list,{'a4@a.fr'})
		
		self.assertEqual(l._BanMgr__white_list,{'a4@a.fr'})
		filtered = l._BanMgr__filterErrors()
		shouldbe = {'ignored'   : { 'robot1' : { 'liste1' : { 'a1@a.fr' : { '5.0.0' : 1 }} }}, 
					'permanent' : { 'robot1' : { 'liste1' : { 'a2@a.fr' : { '5.1.1' : 2 }} }},
					'temporary' : { 'robot1' : { 'liste1' : { 'a3@a.fr' : { '4.2.2' : 1 }},}}
					}
		self.assertEqual(filtered, shouldbe)

#@unittest.skip("bouh")
class TestBanMgr2(unittest.TestCase):
	'''Testing unbanSuccess and banErrors methods'''
	
	def setUp(self):
		_sympa   = Mysql("SYMPARW")
		_symsp = Mysql("SYMSPAM")

		_sympa.request("TRUNCATE TABLE logs_table")
		_symsp.request("TRUNCATE TABLE ban")
		
		d  = []
		self.__d = d
		d0 = Date().getTimestamp() - 7*DAY # One week ago
		for i in range(0,8):
			d.append(Date(d0 + i*DAY))

		# a1 = perm error then success   ==> not banned
		# a2 = success then temp error   ==> not banned
		# a3 = banned, then success 	 ==> unbanned
		# a4 = success only         	 ==> not banned
		# a5 = temp error           	 ==> banned
		# a6 = perm error           	 ==> banned
		# a7 = ignored error       		 ==> not banned
		
		# a1 = perm error then success
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a1@a.fr')",(d[1].getTimestamp(),))
		#_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
		#                   VALUES ('',%s,NULL,'liste1','robot1','success','a1@a.fr')",(d[1].getTimestamp()+10000,))
		_symsp.request("INSERT INTO `whitelist` (date,counter,target_email) VALUES (%s,%s,%s)",(d[1].getIsoformat(),4,'a1@a.fr'))
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber) \
							VALUES ( 'liste1','a1@a.fr','robot1')")

		# a2 = success then temp error

		_symsp.request("INSERT INTO `whitelist` (date,counter,target_email) VALUES (%s,%s,%s)",(d[2].getIsoformat(),4,'a2@a.fr'))
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('',%s,NULL,'liste1','robot1','success','a2@a.fr')",(d[2].getTimestamp(),))
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'4.2.2','liste1','robot1','error','a2@a.fr')",(d[2].getTimestamp()+15000,))
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber) \
							VALUES ( 'liste1','a2@a.fr','robot1')")

		# a3 = banned, then success
		#_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
		#					VALUES ('',%s,NULL,'liste1','robot1','success','a3@a.fr')",(d[2].getTimestamp()+20000,))
		_symsp.request("INSERT INTO `whitelist` (date,counter,target_email) VALUES (%s,%s,%s)",(d[2].getIsoformat(),1,'a3@a.fr'))
		_symsp.request("INSERT INTO ban (robot,list,target_email,date,duration,flag) \
							VALUES ( 'robot1','liste1','a3@a.fr',%s,7,1)",(d[0].getTimestamp()-7*DAY,))
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber) \
							VALUES ( 'liste1','a3@a.fr','robot1')")
		
		# a4 = success
		#_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
		#					VALUES ('',%s,NULL,'liste1','robot1','success','a4@a.fr')",(d[3].getTimestamp()+10000,))
		_symsp.request("INSERT INTO `whitelist` (date,counter,target_email) VALUES (%s,%s,%s)",(d[3].getIsoformat(),4,'a4@a.fr'))
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber) \
							VALUES ( 'liste1','a4@a.fr','robot1')")

		# a5 = temp error
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'4.2.2','liste1','robot1','error','a5@a.fr')",(d[4].getTimestamp(),))
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber) \
							VALUES ( 'liste1','a5@a.fr','robot1')")
								
		# a6 = perm error
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'5.1.1','liste1','robot1','error','a6@a.fr')",(d[5].getTimestamp(),))
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber) \
							VALUES ( 'liste1','a6@a.fr','robot1')")

		# a7 = ignored error
		_sympa.request("INSERT INTO `logs_table` (daemon_logs,date_logs,error_type_logs,list_logs,robot_logs,status_logs,target_email_logs) \
							VALUES ('bounced',%s,'5.0.0','liste1','robot1','error','a7@a.fr')",(d[5].getTimestamp()+DAY/2,))
		_sympa.request("INSERT INTO subscriber_table (list_subscriber,user_subscriber,robot_subscriber) \
							VALUES ( 'liste1','a7@a.fr','robot1')")

		_sympa.commit()
		_symsp.commit()
		
	def test01(self):
		
		d = self.__d

		_sympa = Mysql("SYMPARW")
		_symsp = Mysql("SYMSPAM")
		bl    = BanList(_sympa,_symsp)
		
		self.assertEqual(bl.isBanned('robot1','liste1','a1@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),True)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a3@a.fr'),7)
		self.assertEqual(bl.isBanned('robot1','liste1','a4@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a5@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a6@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a7@a.fr'),False)
		
		# Nothing to unBan, no error to filter
		bm = BanMgr(_sympa,_symsp,d[6],d[7])
		self.assertEqual(bm.unbanSuccess(),0)
		self.assertEqual(bm.banErrors(),(0,0))

		self.assertEqual(bl.isBanned('robot1','liste1','a1@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),True)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a3@a.fr'),7)
		self.assertEqual(bl.isBanned('robot1','liste1','a4@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a5@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a6@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a7@a.fr'),False)

		bm = BanMgr(_sympa,_symsp,d[0],d[7])
		self.assertEqual(bm.unbanSuccess(),1)
		
		self.assertEqual(bl.isBanned('robot1','liste1','a1@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a4@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a5@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a6@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a7@a.fr'),False)

		self.assertEqual(bm.banErrors(),(1,1))
		
		self.assertEqual(bl.isBanned('robot1','liste1','a1@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a2@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a3@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a4@a.fr'),False)
		self.assertEqual(bl.isBanned('robot1','liste1','a5@a.fr'),True)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a5@a.fr'),7)
		self.assertEqual(bl.isBanned('robot1','liste1','a6@a.fr'),True)
		self.assertEqual(bl.getBanDuration('robot1','liste1','a6@a.fr'),10000)
		self.assertEqual(bl.isBanned('robot1','liste1','a7@a.fr'),False)
		
if __name__ == '__main__':
	create_tables()
	unittest.main()
