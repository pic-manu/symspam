#! /usr/bin/python
#
# This file is part of the symspam software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE symspamconf = Definition de la fonction getConf()

version = '0.1'


CONFIG  = {
		"SYMPARW_DB"     : "sympat",
		"SYMPARW_DBPASS" : "QWERTY123",
		"SYMPARW_DBUSER" : "sympat",
		"SYMPARW_DBHOST" : "localhost",

		"SYMPARO_DB"     : "sympat",
		"SYMPARO_DBPASS" : "qwerty123",
		"SYMPARO_DBUSER" : "sympatro",
		"SYMPARO_DBHOST" : "localhost",

		"SYMSPAM_DB"     : "symspamt",
		"SYMSPAM_DBPASS" : "azerty123",
		"SYMSPAM_DBUSER" : "symspamt",
		"SYMSPAM_DBHOST" : "localhost",
}

def getConf(prm):
	'''renvoie la valeur du parametre de config passe en parametres, ou None'''
	try:
		return CONFIG[prm]

	except:
		return None



# TEST
if __name__ == '__main__':
	print(getConf('DB'))
	print(getConf('NAWAK'))

